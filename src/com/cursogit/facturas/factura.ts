import { Estados } from "./estados";
import { IFactura } from "./ifactura";

export class Factura implements IFactura {
    num: number;
    base: number;
    tipoIva: number;
    protected estado: Estados;

    main(): void {

    }

    cambiarEstado(estado: Estados): void { }
    getTotal(): number { return 100; }
    getCantidadIva(): number { return 1000; }
}