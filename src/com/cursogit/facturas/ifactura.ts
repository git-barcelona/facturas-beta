import { Estados } from "./estados";

export interface IFactura {
    cambiarEstado(estado:Estados):void;
    getTotal():number;
    getCantidadIva():number;
  }